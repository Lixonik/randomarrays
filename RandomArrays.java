/*
- Создайте массив из 8 случайных целых чисел из отрезка [1;10]
- Выведите массив на экран в строку
- Далее определите и выведите на экран сообщение о том, является ли массив строго возрастающей последовательностью
- Замените каждый элемент с нечётным индексом на ноль
- Снова выведете массив на экран на отдельной строке
*/

import java.util.Random;
import java.util.Arrays;

public class RandomArrays {
    public static void main(String[] args) {
        int[] randomNumbers = new int[8];
        Random randomNumbersGenerator = new Random();
        for (int i = 0; i < randomNumbers.length; i++) {
            randomNumbers[i] = randomNumbersGenerator.nextInt(10) + 1;
        }
        System.out.println(Arrays.toString(randomNumbers));

        boolean isStrictlyIncremental = true;
        for (int i = 1; i < randomNumbers.length; i++) {
            if (randomNumbers[i] <= randomNumbers[i-1]) {
                isStrictlyIncremental = false;
                break;
            }
        }
        System.out.println(isStrictlyIncremental ? "Последовательность строго возрастающая" : "Последовательность не строго возрастающая");

        for (int i = 0; i < randomNumbers.length; i++) {
            if (i % 2 == 1) {
                randomNumbers[i] = 0;
            }
        }
        System.out.println(Arrays.toString(randomNumbers));
    }
}
